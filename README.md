# Distribution GNU/Linux VireGUL

**Objectifs**

Proposer une distribution Linux clef en main aux adherents de VireGUL contenant :
* Logiciels choisis
* Documentation
* Ressources libres

Voir la documentation des pré-recquis :
https://w.viregul.fr/doku.php?id=projets:distriblinuxviregul
