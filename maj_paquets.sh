# Script mise à jour d'une distribution Linux pour la rendre conforme aux besoins

PHRASEINFO="Enter pour valider ou \"n\" pour passer au suivant"


echo "Pret pour le lancement de la mise à jour ? pressez Entrer ou Ctrl C pour annuler"
read Bidon

echo "Suppression langues inutiles sauf fr"
apt-get purge firefox-locale-de firefox-locale-en firefox-locale-es firefox-locale-it firefox-locale-pt firefox-locale-ru firefox-locale-zh-hans thunderbird-locale-de thunderbird-locale-en thunderbird-locale-en-gb thunderbird-locale-en-us thunderbird-locale-es thunderbird-locale-es-ar thunderbird-locale-es-es thunderbird-locale-it thunderbird-locale-pt thunderbird-locale-pt-br thunderbird-locale-pt-pt thunderbird-locale-ru thunderbird-locale-zh-cn thunderbird-locale-zh-hans thunderbird-locale-zh-hant thunderbird-locale-zh-tw 

echo "copie des fonds d'écran VireGUL"
cp -R paquets/backgrounds-viregul/ /usr/share/backgrounds/

echo "copie des parametres de fonds decran"
mkdir -p /etc/dconf/profile /etc/dconf/db/site.d
cp paquets/user /etc/dconf/profile/
cp paquets/00_bg_settings /etc/dconf/db/site.d/
mkdir -p /etc/skel/Bureau/Documentation
mkdir -p /etc/skel/Desktop/Documentation
mkdir /usr/share/ubuntu-doc-viregul/
cp -r paquets/doc/* /usr/share/ubuntu-doc-viregul/
ln -s /usr/share/ubuntu-doc-viregul/ /etc/skel/Bureau/Documentation/
ln -s /usr/share/ubuntu-doc-viregul/ /etc/skel/Desktop/Documentation/
cp -r paquets/.config /etc/skel/
dconf update

echo "Deb autonomes à installer : pdfsam, qstopmotion ?"
echo $PHRASEINFO
read reponse
if [[ $reponse != "n" ]]; then
# Paquets Bureautique
mkdir deb
cd deb
wget https://github.com/torakiki/pdfsam/releases/download/v3.3.5/pdfsam_3.3.5-1_all.deb
#wget https://code-industry.net/public/master-pdf-editor-4.3.89_qt5.amd64.deb
wget https://freefr.dl.sourceforge.net/project/qstopmotion/Version_2_4_0/qstopmotion-2.4.0-Ubuntu16.04-amd64.deb
# dpkg -i *.deb
apt -f install
rm *.deb
cd ..
rm -rf deb/
fi


# echo "Openjdk 8 : Enter  or Ctrl C"
# read Bidon

#paquets techniques pour pdfsam
#apt install openjdk-8-jre openjdk-8-jre-headless openjfx libqt5printsupport5 libqt5xml5 libqwt-qt5-6 qt5-image-formats-plugins

echo "* Paquets Lang fr *"
echo $PHRASEINFO
read reponse
if [[ $reponse != "n" ]]; then
apt install mythes-fr hyphen-fr hunspell-fr wfrench manpages-fr manpages-fr-extra
fi

echo "* Système et outils techniques : git clamav *"
echo $PHRASEINFO
read reponse
if [[ $reponse != "n" ]]; then
apt install git clamav clamtk clamav-freshclam
fi


echo "* Petits outils accessoires : pdfmod geany, flsint, meld, tomboy, keepassxc, bleachit *"
echo $PHRASEINFO
read reponse
if [[ $reponse != "n" ]]; then
apt install pdfmod p7zip geany geany-plugins fslint vim grsync meld tomboy keepassxc bleachbit
fi


echo "* Internet-fr : Thunderbird, firefox fr pidgin*"
echo $PHRASEINFO
read reponse
if [[ $reponse != "n" ]]; then
apt install thunderbird thunderbird-locale-fr firefox firefox-locale-fr pidgin
fi

echo "* Images : Gimp, pinta, gthumb, inkscape, shutter*"
echo $PHRASEINFO
read reponse
if [[ $reponse != "n" ]]; then
# add-apt-repository -y ppa:shutter/ppa
# apt update
apt install imagemagick pinta  gimp gimp-help-fr gthumb inkscape shutter libgoocanvas2-perl
fi

echo "* Audio & Vidéo : audacity, audacious, easytag, handbrake*"
echo $PHRASEINFO
read reponse
if [[ $reponse != "n" ]]; then
apt install audacity audacious kdenlive soundconverter easytag transmageddon handbrake ffmpeg
fi

echo "* Libreoffice Bureautique stable 6.0 *"
echo $PHRASEINFO
read reponse
if [[ $reponse != "n" ]]; then
add-apt-repository -y ppa:libreoffice/ppa
apt update
apt install libreoffice-l10n-fr libreoffice-help-fr libreoffice-style-human libreoffice-style-oxygen libreoffice-style-elementary libreoffice-style-sifr libreoffice-style-galaxy libreoffice-style-hicontrast openclipart-libreoffice
apt install freeplane scribus dia dia-shapes xournal
fi

echo "* Remmina *"
echo $PHRASEINFO
read reponse
if [[ $reponse != "n" ]]; then
apt-add-repository -y ppa:remmina-ppa-team/remmina-next
apt update
apt install remmina remmina-plugin-rdp  remmina-plugin-vnc
fi

echo "* Jeux Peda Education Gcompris, tuxpaint, scratch, games, mintetest*"
echo $PHRASEINFO
read reponse
if [[ $reponse != "n" ]]; then
apt install gcompris tuxpaint tuxpaint-stamps-default tuxpaint-plugins-default scratch gnome-games gnome-chess ri-li minetest klavaro
fi

echo "* Calibre *"
echo $PHRASEINFO
read reponse
if [[ $reponse != "n" ]]; then
wget -nv -O- https://download.calibre-ebook.com/linux-installer.py | python -c "import sys; main=lambda:sys.stderr.write('Download failed\n'); exec(sys.stdin.read()); main()"
fi

echo "Fonts supplementaires : Enter or Ctrl C"
echo $PHRASEINFO
read reponse
if [[ $reponse != "n" ]]; then
apt install fonts-crosextra-caladea fonts-crosextra-carlito fonts-linuxlibertine typecatcher
fi

# paquets non-libres
#apt install msttcorefonts ubuntu-restricted-extras gstreamer1.0-plugins-ugly gstreamer1.0-plugins-bad

echo "FIN : C'est terminé ! Enter"
read Bidon

#Ajouter gramalecte + ublock + extension Virtualbox pour utilisateur
#wget http://download.virtualbox.org/virtualbox/5.2.8/Oracle_VM_VirtualBox_Extension_Pack-5.2.8-121009.vbox-extpack
#wget http://www.dicollecte.org/grammalecte/oxt/Grammalecte-fr-v0.6.2.oxt
#wget https://addons.mozilla.org/firefox/downloads/file/911040/ublock_origin-1.15.24-an+fx.xpi

